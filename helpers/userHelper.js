var bcrypt = require('bcrypt');
var cryptojs = require('crypto-js');
var jwt = require('jsonwebtoken');
var _ = require('underscore');
var db = require('../db.js');
var secrets = require('../secrets.json');
var cryptoKey = process.env.CRYPTO_KEY || secrets.crypto;
var jwtKey = process.env.JWT_KEY || secrets.jwt;

exports.authenticate = (body) => {
	return new Promise(function(resolve, reject) {

		if (typeof body.username !== 'string' || typeof body.password !== 'string') {
			return reject();
		}

		//See if a user exists with a username
		db.user.findOne({
			username: body.username
		}, function(err, user){
			if(err || !user){
				return reject();
			}

			//Check if the user is nil and if the password given matches the password hash in the db for the found user
			if (!bcrypt.compareSync(body.password, user.password_hash)) {
				return reject({message: "Credentials Wrong"});
			}

			//The password and email given match a user in the database so we pass the user back
			resolve(user);

		});
	});
};

exports.generateToken = (type, user) => {
	if (!_.isString(type)) {
		return undefined;
	}

	try {

		//Turn the json object of the current user's id and the type of token into a string
		var stringData = JSON.stringify({
			_id: user._id,
			type: type
		});

		//Take the json string and encrypt it with a secret then turn it back into a string
		var encryptedData = cryptojs.AES.encrypt(stringData, cryptoKey).toString();

		//Take the encryptedData and turn it into a token with a secret
		var token = jwt.sign({
			token: encryptedData
		}, jwtKey);

		return token;
	} catch(e) {
		return undefined;
	}
};