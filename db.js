var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var env = process.env.APP_ENVIRONMENT || 'development';
var db = {};
console.log(env);

//Check if it is being used in productions or not
if(env === 'production'){
	//Connect to the datbase in amazon
	mongoose.connect(process.env.DATABASE_URL, function(err){
		if(err) throw err;
	});
}else{
	mongoose.connect('mongodb://localhost/localMongo', function(err){
		if(err) throw err;
	});
}

db.user = mongoose.model('User', require('./models/user.js'));
db.token = mongoose.model('Token',require('./models/token.js'));
db.pendingLocation = mongoose.model('PendingLocation', require('./models/pendingLocation.js'));
db.location = mongoose.model('Location', require('./models/location.js'));
db.post = mongoose.model('Post', require('./models/post.js'));
db.vote = mongoose.model('Vote', require('./models/vote.js'));

module.exports = db;
