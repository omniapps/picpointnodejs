var express = require('express');
var bodyParser = require('body-parser');
var _ = require('underscore');
var db = require('./db.js');
var middleware = require('./middleware.js')(db);

//Setting up app
var app = express();
var PORT = process.env.PORT || 8080;

//Apply middleware
app.use(bodyParser.json());

//Controllers
var userController = require('./controllers/userController');
var pendingLocationController = require('./controllers/pendingLocationController');
var locationController = require('./controllers/locationController');
var postController = require('./controllers/postController');
var voteController = require('./controllers/voteController');

//Home route
app.get("/", function(req, res){
	res.send("Welcome to PicPoint!");
});

//--USER API ROUTES--\\
app.post('/user', userController.signUp);
app.post('/user/login', userController.logIn);
app.delete('/user/logout', middleware.requireAuthentication, userController.logOut); 
app.put('/user/update/description', middleware.requireAuthentication, userController.updateDescription);
app.get('/user/badge/information', middleware.requireAuthentication, userController.getUserWithPostsForBadge);
app.get('/user/:badge', middleware.requireAuthentication, userController.getPublicBadgeForUser);
app.post('/user/badge/image', middleware.requireAuthentication, userController.setProfilePicture);

//--PENDING LOCATION API ROUTES--\\
app.post('/pending/location', middleware.requireAuthentication, pendingLocationController.addPendingLocation);
app.post('/pending/location/confirm', middleware.requireAuthentication, pendingLocationController.confirmPendingLocation);
app.get('/pending/locations/:loc', middleware.requireAuthentication, pendingLocationController.getPendingLocationsNearMe);
app.post('/pending/location/flag', middleware.requireAuthentication, pendingLocationController.flagPendingLocation);

//--LOCATION API ROUTES--\\
app.get('/locations/:loc', middleware.requireAuthentication, locationController.getLocationsNearMe);
app.post('/locations/loc/more', middleware.requireAuthentication, locationController.getMoreLocationsNearMe);
app.get('/locations/top/global', middleware.requireAuthentication, locationController.getGlobalLocations);
app.post('/locations/top/global/more', middleware.requireAuthentication, locationController.getMoreGlobalLocations);
app.get('/locations/search', middleware.requireAuthentication, locationController.searchLocations);
app.post('/location/flag', middleware.requireAuthentication, locationController.flagLocation);
app.get('/locations/mapview/:map', middleware.requireAuthentication, locationController.getLocationsForMap);

//--POST API ROUTES--\\
app.post('/post', middleware.requireAuthentication, postController.post);
app.get('/posts/:loc', middleware.requireAuthentication, postController.getPostsInLocation);
app.post('/posts/loc/more', middleware.requireAuthentication, postController.getMorePostsInLocation);
app.get('/user/posts', middleware.requireAuthentication, postController.getUsersPosts);
app.post('/post/delete', middleware.requireAuthentication, postController.deletePost);
app.post('/post/flag', middleware.requireAuthentication, postController.flagPost);

//--VOTE API ROUTES--\\
app.post('/post/vote', middleware.requireAuthentication, voteController.vote);

app.listen(PORT, function(){
	console.log('Listening on port: ' + PORT);
});

//500 is internal error
//400 is bad request
//404 is not found
//204 is a good not found
