var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
	imageExt: {
		type: String,
		required: true
	},
	userId: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true
	},
	locId: {
		type: String,
		required: true
	},
	locName: {
		type: String,
		required: true
	},
	numberOfVotes: {
		type: Number,
		required: true,
		default: 0
	},
	description: {
		type: String,
		required: false,
		default: ""
	},
	flagged: {
		type: Boolean,
		required: true,
		default: false
	}
});

module.exports = postSchema;