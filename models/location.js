var mongoose = require('mongoose');

var locationSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: false
	},
	loc: {
		type: [Number], //[<long>,<lat>]
		index: '2d'	,
		required: true,	//create a geospatial index
		validate: [checkCoords, 'Coordinates were incorrect']
	},
	numberOfPosts: {
		type: Number,
		default: 0,
		required: true
	},
	flagged: {
		type: Boolean,
		required: true,
		default: false
	},
	imageExt: {
		type: String,
		required: true,
		default: ""
	}
});

function checkCoords(v) {
	if(v[0].length < -180 || v[0].length > 180){
		return false;
	}else if(v[1].length < -90 || v[1].length > 90){
		return false;
	}else{
		return true;
	}
};

module.exports = locationSchema;