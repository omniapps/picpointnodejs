var mongoose = require('mongoose');

var pendingLocationSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: false
	},
	loc: {
		type: [Number], //[<long>,<lat>]
		index: '2d'	,
		required: true,	//create a geospatial index
		validate: [checkCoords, 'Coordinates were incorrect']
	},
	pendingCount: {
		type: Number,
		required: true,
		default: 1
	},
	userId: {
		type: String,
		required: true
	},
	flagged: {
		type: Boolean,
		required: true,
		default: false
	},
	imageExt: {
		type: String,
		required: true
	}
});

function checkCoords(v) {
	if(v[0] < -180 || v[0] > 180){
		return false;
	}else if(v[1] < -90 || v[1] > 90){
		return false;
	}else{
		return true;
	}
};

module.exports = pendingLocationSchema;