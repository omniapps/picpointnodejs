var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var _ = require('underscore');
var cryptojs = require('crypto-js');
var jwt = require('jsonwebtoken');

var userSchema = mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true,
		validate: [checkUsername, 'Username was wrong format']
	},
	email: {
		type: String,
		required: true,
		unique: true,
		validate: [checkEmail, 'Email was wrong format']
	},
	salt: {
		type: String,
		required: true
	},
	password_hash: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	numberOfVotes: {
		type: Number,
		required: true,
		default: 0
	},
	profileImageExt: {
		type: String,
		required: false,
		default: ""
	}
});

//Salt and hash the password using this virtual setter
userSchema.virtual('password').set(function(v) {

	//We want to check the correct length of the given password
	if(v.length < 6  || v.length > 25){
		return;
	}

	//generate a salt with x amount of characters.. 10 in this case
	var salt = bcrypt.genSaltSync(10);

	//Hash the password with the salt and original password
	var hashedPassword = bcrypt.hashSync(v, salt);

	//Set all of the properties for the user
	this.salt = salt;
	this.password_hash = hashedPassword;
});

//Check for correct username format
function checkUsername(v) {
	if(typeof v === 'string') {
		if(v.trim().length > 0 & v.trim().length < 16){
			this.username = v.trim().toLowerCase();
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
};

//Check for correct email format. Eventually I will need to check for email @ sign and .com
function checkEmail(v) {
	if(typeof v === 'string') {
		if(v.trim().length > 0){
			this.email = v.trim().toLowerCase();
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
};

//Make sure to add a check to see if the email and username are strings and if so lowercase them

module.exports = userSchema;