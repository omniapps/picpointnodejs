var mongoose = require('mongoose');
var cryptojs = require('crypto-js');

var tokenSchema = mongoose.Schema({
	tokenHash: {
		type: String,
		required: true,
		
	}
});

//Hash the token
tokenSchema.virtual('token').set(function(v) {

	//We dont want an empty token
	if(v.length < 1){
		return;
	}

	//Use cryptojs MD5 hashing alg
	var hash = cryptojs.MD5(v).toString();

	this.tokenHash = hash;
	
});

module.exports = tokenSchema;