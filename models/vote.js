var mongoose = require('mongoose');

var voteSchema = mongoose.Schema({
	voteValue: {
		type: Number,
		required: true,
		validate: [checkVoteValue, 'Incorrect vote value']
	},
	userId: {
		type: String,
		required: true
	},
	postId: {
		type: String,
		required: true
	},
	locId: {
		type: String,
		required: true
	}
});

function checkVoteValue(v) {
	if(v !== 1 && v !== -1){
		return false;
	}else{
		return true;
	}
};

module.exports = voteSchema;