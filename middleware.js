var cryptojs = require('crypto-js');
var db = require('./db.js');
var mongoose = require('mongoose');
var _ = require('underscore');
var jwt = require('jsonwebtoken');
var secrets = require('./secrets.json');
var cryptoKey = process.env.CRYPTO_KEY || secrets.crypto;
var jwtKey = process.env.JWT_KEY || secrets.jwt;

//Require the db to be passed in
module.exports = function(db){

	return {
		//Middleware or in this case requireAuthentication will run before the REST API methods main code
		//Next is used to move on and run the REST API method of choice
		//requireAuthentication will be used in every route that is obviously in need of authentication to do that specific action
		//We take the token that is passed in the request and find a token hash that matches it in the db
		//If the token exists then we use our custom classMethod (findByToken) for user to find the user by the token
		//Then we pass the user back in the request so the main code for each route can use the user
		requireAuthentication: function(req, res, next) {
			//We now have the token in the Auth header 
			//Using || '' makes sure it never crashes
			var token = req.get('Auth') || '';

			//Turn the token into a hash and search for one
			db.token.findOne({
				tokenHash: cryptojs.MD5(token).toString()
			}, function(err, tokenInstance) {
				if(err) {
					return res.status(500).send();
				}else if(!tokenInstance) {
					//We should only need this in log out. In the event that the token is not found... that is bad
					//That means they requested to do something and a token given did not match one in the database
					return res.status(502).send();
				}

				//Set the token in the request to the token instance. the tokenInstance is actually a token hash
				//We put this here so we can call .destroy in the logout route
				req.token = tokenInstance;

				//Find a user by a token
				//This could be put this as a user model method...thats for later
				try {
					//Verify the token is not modified
					var decodedJMT = jwt.verify(token, jwtKey);

					//Decrypt data into bytes
					var bytes = cryptojs.AES.decrypt(decodedJMT.token, cryptoKey);

					//Convert the string of bytes into a json object
					var tokenData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));

					//Find user by id with token data
					db.user.findOne({
						_id: tokenData._id
					}, function(err, user){
						if(err || !user){
							return res.status(500).send();
						}

						//No error so put the user on req.user
						req.user = user;
						next();
					})

				} catch(e) {
					//Do something ith the error
					return res.status(500).send();
				}
			});
		}
	};
};