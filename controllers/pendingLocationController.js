var db = require('../db.js');
var _ = require('underscore');
var aws = require('aws-sdk');
var secrets = require('../secrets.json');
var accessKeyId = process.env.S3_ACCESS_KEY_ID || secrets.accessKeyId;
var secretAccessKey = process.env.S3_SECRET_ACCESS_KEY || secrets.secretAccessKey;
var bucketName = process.env.BUCKET_NAME || secrets.bucketName;

aws.config.update({
	accessKeyId: accessKeyId,
	secretAccessKey: secretAccessKey,
	region: 'us-west-2'
});

exports.addPendingLocation = (req, res) => {
	var body = _.pick(req.body, "name", "longitude", "latitude");

	if (!body.name || !body.longitude || !body.latitude) {
		return res.status(400).send();
	}

	var s3 = new aws.S3({
		signatureVersion: 'v4'
	});

	//Get the current date and time for unique pendingLocation
	var currentdate = new Date();
	var datetime = "" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "-" + currentdate.getFullYear() + "-" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

	var createdExt = 'pendingLocation/' + req.user.username + "@" + datetime + '.jpeg'

	//Set the url to expire in 60 seconds
	//Create a key that is the extension for the image
	var params = {
		Bucket: bucketName,
		Key: createdExt,
		Expires: 60
	};

	s3.getSignedUrl('putObject', params, function(err, url) {
		if (!url || err) {
			return res.status(500).send();
		}

		var pendingLocation = db.pendingLocation({
			name: body.name,
			loc: [body.longitude, body.latitude],
			userId: req.user._id,
			imageExt: createdExt
		});

		pendingLocation.save(function(err, pendingLocation) {
			if (err || !pendingLocation) {
				res.status(500).send(err.errmsg);
			}

			res.json({
				imageURL: url
			});
		});
	});
};

//Find a pending location where the id matches, create a location because if this request is fired it is the second person
//to upvote the location
exports.confirmPendingLocation = (req, res) => {
	var body = _.pick(req.body, "_id");

	if(!body._id){
		return res.status(400).send();
	}

	//We need to find a pending location by the id and make sure the user that made the pending location can not also confirm it
	//Then we delete it
	db.pendingLocation.findOneAndRemove({
		_id: body._id,
		userId: {
			$ne: req.user._id
		}
	}, function(err, pendingLocation){
		if(err){
			return res.status(400).send();
		}else if(!pendingLocation){
			return res.status(404).send();
		}

		//We found the pendingLocation so now we need to make an acutal location
		var location = db.location({
			name: pendingLocation.name,
			loc: pendingLocation.loc,
			imageExt: pendingLocation.imageExt
		});

		location.save(function(err){
			if(err){
				return res.status(500).send();
			}

			res.json({
				message: "Location created"
			});
		});

	});
};

//Need to add getPendingLocationsNearMe
exports.getPendingLocationsNearMe = (req, res) => {
	var query = req.query

	if(!query.hasOwnProperty('longitude') || !query.hasOwnProperty('latitude')){
		return res.status(400).send();
	}
	
	//Establish a max distance and convert it (currently in km, 400 feet)
	var maxDistance = 0.12192;

	//1 degree = 111.12 
	//We need to turn km to radians using that num
	maxDistance /= 111.12;

	db.pendingLocation.find({
		loc: {
			$near: [query.longitude, query.latitude],
			$maxDistance: maxDistance
		},
		flagged: false
	}, function(err, pendingLocations){
		if(err){
			return res.status(500).send();
		}
		res.json(pendingLocations);
	});
};

exports.flagPendingLocation = (req, res) => {
	var body = _.pick(req.body, '_id');

	if(!body._id){
		return res.status(400).send();
	}

	db.pendingLocation.findOneAndUpdate({
		_id: body._id
	}, {
		flagged: true
	}, function(err, pendingLocation){
		if(err || !pendingLocation){
			return res.status(500).send();
		}

		res.json({
			message: 'Flagged pending location'
		});
	});
};
