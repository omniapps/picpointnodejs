var db = require('../db.js');
var _ = require('underscore');

exports.getLocationsNearMe = (req, res) => {
	var query = req.query

	if(!query.hasOwnProperty('longitude') || !query.hasOwnProperty('latitude')){
		return res.status(400).send();
	}

	//Establish a max distance and convert it (currently in km, 400 feet)
	var maxDistance = 0.12192;

	//1 degree = 111.12 
	//We need to turn km to radians using that num
	maxDistance /= 111.12;

	db.location.find({
		loc: {
			$near: [query.longitude, query.latitude],
			$maxDistance: maxDistance
		},
		flagged: false
	}).sort({
		numberOfPosts: -1
	}).limit(10).exec(function(err, locations){
		if(err){
			return res.status(500).send();
		}

		res.json(locations);
	});
};

//This is to enable pagination on locations
exports.getMoreLocationsNearMe = (req, res) => {
	var body = _.pick(req.body, "locationIds", "numberOfPosts", "longitude", "latitude");

	//CHECK IF !LONGITUDE MEANS 0 OR THAT IT DOESNT HAVE THE PROPERTY
	if(!body.locationIds || !body.hasOwnProperty('numberOfPosts') || !body.longitude || !body.latitude){
		return res.status(400).send();
	}

	//Establish a max distance and convert it (currently in km, 400 feet)
	var maxDistance = 0.12192;

	//1 degree = 111.12 
	//We need to turn km to radians using that num
	maxDistance /= 111.12;

	db.location.find({
		loc: {
			$near: [body.longitude, body.latitude],
			$maxDistance: maxDistance
		},
		numberOfPosts: {
			$lte: body.numberOfPosts
		},
		_id: {
			$nin: body.locationIds
		},
		flagged: false
	}).sort('-numberOfPosts').limit(10).exec(function(err, locations){
		if(err){
			return res.status(500).send();
		}

		res.json(locations);
	});
};

//This is just to show users what pins are around them
exports.getLocationsForMap = (req, res) => {
	var query = req.query;

	if(!query.hasOwnProperty('longitude') || !query.hasOwnProperty('latitude')){
		return res.status(400).send();
	}

	//Establish a max distance and convert it (2.5m to km)
	var maxDistance = 4.02336;

	//1 degree = 111.12 
	//We need to turn km to radians using that num
	maxDistance /= 111.12;

	db.location.find({
		loc: {
			$near: [query.longitude, query.latitude],
			$maxDistance: maxDistance
		},
		flagged: false
	}).exec(function(err, locations){
		if(err){
			return res.status(500).send();
		}

		res.json(locations);
	});
};

//This is for the searching of locations arround the world
exports.searchLocations = (req, res) => {
	var query = req.query;

	if(!query.hasOwnProperty('locName')){
		return res.status(400).send();
	}

	//We need to use regex to make sure we query users that start with the letters of locName given
	var regexName = new RegExp("^" + query.locName);

	db.location.find({
		name: regexName,
		flagged: false
	}).sort({
		numberOfPosts: -1
	}).limit(10).exec(function(err, locations){
		if(err){
			return res.status(500).send();
		}

		res.json(locations);
	});
};

//Get the top 20 locations around the world sorted by the number of posts
exports.getGlobalLocations = (req, res) => {
	db.location.find({
		flagged: false
	}).sort({
		numberOfPosts: -1
	}).limit(20).exec(function(err, locations){
		if(err){
			return res.status(500).send();
		}

		res.json(locations);
	});
};

//This is to enable pagination on locations
exports.getMoreGlobalLocations = (req, res) => {
	var body = _.pick(req.body, "locationIds", "numberOfPosts");

	if(!body.locationIds || !body.hasOwnProperty('numberOfPosts')){
		return res.status(400).send();
	}

	db.location.find({
		numberOfPosts: {
			$lte: body.numberOfPosts
		},
		_id: {
			$nin: body.locationIds
		},
		flagged: false
	}).sort('-numberOfPosts').limit(10).exec(function(err, locations){
		if(err){
			console.log(err);
			return res.status(500).send();
		}

		res.json(locations);
	});
};

exports.flagLocation = (req, res) => {
	var body = _.pick(req.body, '_id');

	if(!body._id){
		return res.status(400).send();
	}

	db.location.findOneAndUpdate({
		_id: body._id
	}, {
		flagged: true
	}, function(err, location){
		if(err || !location){
			return res.status(500).send();
		}

		res.json({
			message: 'Flagged location'
		});
	});
};

