var db = require('../db.js');
var _ = require('underscore');

exports.vote = (req, res) => {
	var body = _.pick(req.body, "postId", "voteValue", "locId");

	if(!body.postId || !body.locId || !body.voteValue || (body.voteValue !== 1 && body.voteValue !== -1)){
		return res.status(400).send();
	}

	//We need to check to see if the user voted already on the post in the location
	db.vote.findOneAndUpdate({
		userId: req.user._id,
		postId: body.postId,
		locId: body.locId
	}, {
		$set: {
			voteValue: body.voteValue
		}
	}, function(err, doc){
		if(err){
			return res.status(500).send();
		}else if(!doc){
			//We could not find one to update so we want to create it
			var vote = db.vote({
				userId: req.user._id,
				postId: body.postId,
				voteValue: body.voteValue,
				locId: body.locId
			});

			vote.save(function(err){
				if(err){
					return res.status(500).send();
				}

				//Now we need to increment the post by the new vote
				db.post.findOneAndUpdate({
					_id: vote.postId
				}, {
					$inc: {
						numberOfVotes: vote.voteValue
					}
				}, {
					new: true
				}, function(err, post){
					if(err || !post){
						return res.status(500).send();
					}

					//After we incremenent the number of votes on the post
					//We want to increment the number of votes the posts owner gets
					db.user.findOneAndUpdate({
						_id: post.userId
					}, {
						$inc: {
							numberOfVotes: vote.voteValue
						}
					}, function(err, doc) {
						if(err || !doc) {
							return res.status(500).send();
						}

						//Return the post after it is all said and done
						return res.json(post);
					});
				});
			});
		}else if(doc){

			//We found a document and updated it
			//If the value given is the save as the original vote value
			//Then we dont need to update the posts
			//For example vote value given is 1 and the vote value given in the request is 1
			//We wouldnt want to increment the same post twice for the same vote document
			//One vote per person per post
			if(body.voteValue === doc.voteValue){
				return res.status(400).send();
			}

			var incAmount;

			if(body.voteValue === -1){
				incAmount = -2;
			}else if(body.voteValue === 1){
				incAmount = 2;
			}

			//Now we need to increment the post by the new vote value of the existing doc
			db.post.findOneAndUpdate({
				_id: doc.postId
			}, {
				$inc: {
					numberOfVotes: incAmount
				}
			}, {
				new: true
			},function(err, post){
				if(err || !post){
					return res.status(500).send();
				}

				//After we incremenent the number of votes on the post
				//We want to increment the number of votes the posts owner gets
				db.user.findOneAndUpdate({
					_id: post.userId
				}, {
					$inc: {
						numberOfVotes: incAmount
					}
				}, function(err, doc) {
					if(err || !doc) {
						return res.status(500).send();
					}
				
					//Return the post after it is all said and done
					return res.json(post);
				});
			});
		}
	});
};