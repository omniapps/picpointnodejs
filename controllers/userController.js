var db = require('../db.js');
var _ = require('underscore');
var helper = require('../helpers/userHelper.js');
var aws = require('aws-sdk');
var secrets = require('../secrets.json');
var accessKeyId = process.env.S3_ACCESS_KEY_ID || secrets.accessKeyId;
var secretAccessKey = process.env.S3_SECRET_ACCESS_KEY || secrets.secretAccessKey;
var bucketName = process.env.BUCKET_NAME || secrets.bucketName;

aws.config.update({
	accessKeyId: accessKeyId,
	secretAccessKey: secretAccessKey,
	region: 'us-west-2'
});
    
exports.signUp = (req, res) => {
 
    //Pick the email, password, username out of the request
	var body = _.pick(req.body, 'email', 'password', 'username');

	if(!body.email || !body.password || !body.username){
		return res.status(400).send();
	}

	//Create the user using the body variable
	var user = db.user({
		username: body.username,
		email: body.email,
		password: body.password
	});

	//Gotta save things in mongo and check to see if certain things are available
	user.save(function(err) {
		if(err == null){
			return res.json({message: "User created"});
		}else if(err.errmsg && err.errmsg.toString().includes("username")){
			return res.status(500).json({message: "Username taken"});
		}else if(err.errmsg && err.errmsg.toString().includes("email")){
			return res.status(500).json({message: "Email taken"});
		}else if(err){
			return res.status(500).json({message: "Error"});
		}
	});
};

exports.logIn = (req, res) => {
    
    //Get their email and password only
	var body = _.pick(req.body, 'username', 'password');

	if(!body.password || !body.username){
		return res.status(400).send();
	}

	//Wanted to lowercase so we dont have someone trying to login as Connor when their username is connor
	body.username = body.username.toLowerCase();

	var userInstance;
	var token;

	//Authenticate user using our helper method
	helper.authenticate(body).then(function(user){

		//User is authenticated so we need to create a token for them using our custom helper method as well
		token = helper.generateToken('authentication', user);

		//Set the instance of the user so we can access it outside of this callback
		userInstance = user;
		
		//Store thie token in the db
		//This token uses its virtual function to hash it and only store the hash
		var newToken = db.token({
			token: token
		});

		newToken.save(function(err) {
			if(err) {
				return res.status(500).send(err.errmsg);
			}else {
				//Set the header in the response to include a key 'Auth' and the value as the token from the token instance 
				res.header('Auth', token).json({username: userInstance.username, _id: userInstance._id, message: "You are logged in"});
				console.log('Logged in');
			}
		});
	}, function(e){
		res.status(500).json(e);
	});
};

exports.logOut = (req, res) => {
	//The token is on req because of the middleware
	//We just have to delete the token in order to log out
	db.token.findOneAndRemove({
		tokenHash: req.token.tokenHash
	}, function(err, token) {
		if(err || !token) {
			return res.status(500).send();
		}
		
		res.status(200).send();
	});
};

exports.getUserWithPostsForBadge = (req, res) => {
	var userWithPosts = [];

	db.user.findOne({
		_id: req.user._id
	}, function(err, user) {
		if(err || !user) {
			return res.status(500).send();
		}

		pickedUser = _.pick(user, "username", "_id", "numberOfVotes", "description");
		userWithPosts[0] = user;

		db.post.find({
			userId: pickedUser._id
		}).sort({
			numberOfVotes: -1
		}).exec(function(err, posts) {
			if(err) {
				return res.status(500).send()
			}

			userWithPosts[1] = posts;

			//Send back the users with the posts
			res.json(userWithPosts);
		});
	});
};

//Here we are getting all of the public information for a user based on the id given from the post
//This route will be called from clicking on the username of the post
exports.getPublicBadgeForUser = (req, res) => {
	var query = req.query;

	if(!query.hasOwnProperty('userId')){
		return res.status(400).send();
	}

	//Since we want the user and the posts
	//We will want to make an array to pass back
	var userWithPosts = [];

	db.user.findOne({
		_id: query.userId
	}, function(err, user) {
		if(err || !user) {
			return res.status(500).send();
		}

		//Picking the user allows us to remove sensitive data
		pickedUser = _.pick(user, "username", "_id", "numberOfVotes", "description", "profileImageExt");
		userWithPosts[0] = user;

		//After we add the user to the first index of the array
		//We want to add all of their posts to the second index
		//In descending order
		db.post.find({
			userId: pickedUser._id
		}).sort({
			numberOfVotes: -1
		}).exec(function(err, posts) {
			if(err) {
				return res.status(500).send()
			}

			userWithPosts[1] = posts;

			//Send back the users with the posts
			res.json(userWithPosts);
		});
	});
};

exports.updateDescription = (req, res) => {
	var body = _.pick(req.body, 'description');

	if(!body.description){
		return res.status(400).send();
	}

	db.user.findOneAndUpdate({
		_id: req.user._id
	}, {
		description: body.description
	}, function(err, user){
		if(err || !user){
			return res.status(500).send();
		}

		res.json({
			message: "Description updated"
		});
	});
};

exports.setProfilePicture = (req, res) => {
	var s3 = new aws.S3({
		signatureVersion: 'v4'
	});

	//We want to delete the current profile image if there is one before we set a new one
	//The default value for no profile image is ""
	if(req.user.profileImageExt !== "") {
		//Delete it from the bucket before we store a new image
		s3.deleteObject({
			Bucket: bucketName,
			Key: req.user.profileImageExt
		}, function(err) {
			if(err) {
				return res.status(500).send();
			}

			//Get the current date and time for unique image
			var currentdate = new Date();
			var datetime = "" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "-" + currentdate.getFullYear() + "-" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

			var createdExt = 'profilepicture/' + req.user.username + "@" + datetime + '.jpeg'

			//Set the url to expire in 60 seconds
			//Create a key that is the extension for the image
			var params = {
				Bucket: bucketName,
				Key: createdExt,
				Expires: 60
			};

			s3.getSignedUrl('putObject', params, function(err, url) {
				if (!url || err) {
					return res.status(500).send();
				}

				//Now that we have the url we want to update the user and add the imageExt
				db.user.findOneAndUpdate({
					_id: req.user._id
				}, {
					profileImageExt: createdExt
				}, function(err, user){
					if(err || !user){
						return res.status(500).send();
					}

					res.json({
						imageURL: url
					});
				});
			});
		});
	}else {
		//Since there is not already an image for the user, make one
		//Get the current date and time for unique image
		var currentdate = new Date();
		var datetime = "" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "-" + currentdate.getFullYear() + "-" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

		var createdExt = 'profilepicture/' + req.user.username + "@" + datetime + '.jpeg'

		//Set the url to expire in 60 seconds
		//Create a key that is the extension for the image
		var params = {
			Bucket: bucketName,
			Key: createdExt,
			Expires: 60
		};

		s3.getSignedUrl('putObject', params, function(err, url) {
			if (!url || err) {
				return res.status(500).send();
			}

			//Now that we have the url we want to update the user and add the imageExt
			db.user.findOneAndUpdate({
				_id: req.user._id
			}, {
				profileImageExt: createdExt
			}, function(err, user){
				if(err || !user){
					return res.status(500).send();
				}

				res.json({
					imageURL: url
				});
			});
		});
	}

};

