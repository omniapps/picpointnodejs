var db = require('../db.js');
var _ = require('underscore');
var aws = require('aws-sdk');
var secrets = require('../secrets.json');
var accessKeyId = process.env.S3_ACCESS_KEY_ID || secrets.accessKeyId;
var secretAccessKey = process.env.S3_SECRET_ACCESS_KEY || secrets.secretAccessKey;
var bucketName = process.env.BUCKET_NAME || secrets.bucketName;

aws.config.update({
	accessKeyId: accessKeyId,
	secretAccessKey: secretAccessKey,
	region: 'us-west-2'
});


//This takes the locationId given and the needURL attribute as true and returns a presigned url
exports.post = (req, res) => {
	var body = _.pick(req.body, "locId", "locName", "description");
	var user = req.user;

	//We want to make sure the needURL attribute is actually a true boolean
	if (!body.locId || !body.locName || !body.description) {
		return res.status(400).send();
	}

	var s3 = new aws.S3({
		signatureVersion: 'v4'
	});

	//Get the current date and time for unique image
	var currentdate = new Date(); 
	var datetime = "" + (currentdate.getMonth()+1) + "-" + currentdate.getDate() + "-" + currentdate.getFullYear() + "-"  
                + currentdate.getHours() + ":" 
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

    var createdExt = 'post/' + req.user.username + "@" + datetime + '.jpeg'

	//Set the url to expire in 60 seconds
	//Create a key that is the extension for the image
	var params = {
		Bucket: bucketName,
		Key: createdExt,
		Expires: 60
	};

	s3.getSignedUrl('putObject', params, function(err, url) {
		if (!url || err) {
			return res.status(500).send();
		}

		//Could potentially change this to just pass the entire location and user to the schema
		var post = db.post({
			imageExt: createdExt,
			userId: user._id,
			username: user.username,
			locId: body.locId,
			locName: body.locName,
			description: body.description
		});

		saveAndCreatePost(req, res, post, url);
	});
};


//For now this is a temporary callback
function saveAndCreatePost(req, res, post, url) {
	post.save(function(err, newPost){
		if(err || !newPost){
			return res.status(500).send();
		}

		//We made the post but now we want to increment the amount of posts in the location
		db.location.findOneAndUpdate({
			_id: newPost.locId
		}, {
			$inc: {
				numberOfPosts: 1
			}
		}, function(err, location){
			if(err || !location){
				return res.status(500).send();
			}

			//Now that our virtual function got a presigned url after saving, we can pass it back
			res.json({
				imageURL: url
			});
		});
	});
};


//I need to add getting them by most popular and setting a limit of like 10
//We need to make sure it actually gets the top 10 posts. Not just sort the most recent 10
//Also in the future I need to have it where I only get potential votes on the posts given
//Right now it is getting all the votes by a user in the location
exports.getPostsInLocation = (req, res) => {
	var query = req.query
	var postsWithVotes = [];

	if(!query.hasOwnProperty('locId')){
		return res.status(400).send();
	}

	//Find all the posts where the location Id attatched to the post is the same as the location id we got from req
	db.post.find({
		locId: query.locId,
		flagged: false
	}).sort({
		numberOfVotes: -1
	}).limit(10).exec(function(err, posts) {
		if(err){
			console.log(err);
			return res.status(500).send();
		}

		//Get all of the ids in the posts
		var postIdsToCheck = [];

		posts.forEach(function(post){
			postIdsToCheck.push(post._id);
		});

		postsWithVotes[0] = posts;

		//Now we want to add all the votes that the user has on the set of posts
		//ADD NIN TO THIS SO WE CAN ONLY GET THE VOTES FOR THE POSTS
		db.vote.find({
			locId: query.locId,
			userId: req.user._id ,
			postId: {
				$in: postIdsToCheck
			}
		}).lean().exec(function(err, votes){
			if(err){
				console.log(err);
				return res.status(500).send();
			}

			postsWithVotes[1] = votes;

			//We got some posts with all of the votes made by the user in that location
			res.json(postsWithVotes);
		});
	});
};

//This is to enable pagination on posts
exports.getMorePostsInLocation = (req, res) => {
	var body = _.pick(req.body, "numberOfVotes", "locId", "postIds");

	if(!body.locId || !body.hasOwnProperty("numberOfVotes") || !body.postIds){
		console.log(body);
		return res.status(400).send();
	}

	var postsWithVotes = [];

	//Find more posts where 
	db.post.find({
		locId: body.locId,
		numberOfVotes: {
			$lte: body.numberOfVotes
		},
		_id: {
			$nin: body.postIds
		},
		flagged: false
	}).sort('-numberOfVotes').limit(5).lean().exec(function(err, posts) {
		if(err){
			return res.status(500).send();
		}
		
		var postIdsToCheck = body.postIds;
		
		//Go through each post found and add the id to the array of ids to check for votes
		//Basically when users get more, they send us an array of postIds that might contain a vote by the user
		//Since we get more posts including the given postIds, we want to add them all together
		//Now we can get votes that might belong to previous and newly retrieved posts
		posts.forEach(function(x) {
			postIdsToCheck.push(x._id);
		});

		postsWithVotes[0] = posts;

		//Now we want to add all the votes that the user has on the set of posts
		db.vote.find({
			locId: body.locId,
			userId: req.user._id,
			postId: {
				$in: postIdsToCheck
			}
		}).lean().exec(function(err, votes){
			if(err){
				return res.status(500).send();
			}

			postsWithVotes[1] = votes;

			//We got some posts with all of the votes made by the user in that location
			res.json(postsWithVotes);
		});
	});
};

//I need to get all of the posts for the user
exports.getUsersPosts = (req, res) => {
	db.post.find({
		userId: req.user._id
	}).sort({
		numberOfVotes: -1
	}).lean().exec(function(err, posts){
		if(err){
			return res.status(500).send()
		}

		//Send back the posts
		res.json(posts);
	});
};

//Delete a post for a specific user
//Then we decrement the vote count on a location 
//Then delete all the votes on the post
exports.deletePost = (req, res) => {
	var body = _.pick(req.body, '_id');

	if (!body._id) {
		return res.status(400).send();
	}

	db.post.findOneAndRemove({
		_id: body._id,
		userId: req.user._id
	}, function(err, post) {
		if (err || !post) {
			return res.status(500).send();
		}

		//Now lets delete the image from amazon
		var s3 = new aws.S3({
			signatureVersion: 'v4'
		});

		s3.deleteObject({
			Bucket: bucketName,
			Key: post.imageExt
		}, function(err) {
			if (err) {
				return res.status(500).send();
			}

			//We removed the post so we want to decrement the posts on the location
			db.location.findOneAndUpdate({
				_id: post.locId
			}, {
				$inc: {
					numberOfPosts: -1
				}
			}, function(err, location) {
				//We were not able to find the location to update
				if (err || !location) {
					return res.status(500).send();
				}

				//Decremented the location so now we want to delete all the votes
				db.vote.remove({
					postId: post._id
				}, function(err, votes) {
					if (err) {
						return res.status(500).send();
					}

					//The post was removed, the s3 image was deleted, the votes on the post were removed, and the location's numberOfPosts were updated
					res.status(204).json({
						message: "The post was deleted"
					});
				});
			});

		});
	});
};


exports.flagPost = (req, res) => {
	var body = _.pick(req.body, '_id');

	if(!body._id){
		return res.status(400).send();
	}

	db.post.findOneAndUpdate({
		_id: body._id
	}, {
		flagged: true
	}, function(err, post){
		if(err || !post){
			return res.status(500).send();
		}

		res.json({
			message: 'Flagged post'
		});
	});
};















